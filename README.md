# Introduction

According to the bad themes of any IDE, this project represents customized color schemes for diverse IDEs such as Visual Studio and IntelliJ IDEA.

# Installation

To install these themes, see [here for Visual Studio 2010 (and newer)](https://blogs.msdn.microsoft.com/zainnab/2010/07/15/importing-or-changing-your-environment-settings/), see [here for IntelliJ IDEA](https://stackoverflow.com/questions/39187/how-do-i-install-a-color-theme-for-intellij-idea-7-0-x).

# Dark Theme

The used colors are as follows.  

|   Preview	|   Hex color code	|   Usage	|
|---	|---	|---	|
|   ![14](img/14.png)	|   `#252525`	|   Text editor (background)	|
|   ![1](img/1.png)	|   `#569CD6`	|   Keywords  	|
|   ![2](img/2.png)	|   `#B5CEA8`	|   Numbers  	|
|   ![3](img/3.png)	|   `#BDB76B`	|   Local variables  	|
|   ![4](img/4.png)	|   `#D69D85`	|   Strings  	|
|   ![5](img/5.png)	|   `#FF8C00`	|   Function declarations and definitions (ctor and dtor too), function calls and static functions	|
|   ![6](img/6.png)	|   `#3CB371`	|   Types, classes, constructor calls and namespaces  	|
|   ![7](img/7.png)	|   `#BD63C5`	|   Enumerations (macro) and constants  	|
|   ![8](img/8.png)	|   `#00D700`	|   Global variables  	|
|   ![9](img/9.png)	|   `#8F873F`	|   Parameters  	|
|   ![10](img/10.png)	|   `#DADADA`	|   Literals  	|
|   ![11](img/11.png)	|   `#FC3E36`	|   Syntax errors (underline)  	|
|   ![12](img/12.png)	|   `#0E4583`	|   Braces belonging to another braces (background)  	|
|   ![13](img/13.png)	|   `#57A64A`	|   Comments  	|
|   ![15](img/15.png)	|   `#3399FF`	|   Marked text (background)  	|
|   ![16](img/16.png)	|   `#DCDCDC`	|   Text  	|
|   ![17](img/17.png)	|   `#2B91AF`	|   Line numbers  	|
|   ![18](img/18.png)	|   `#B4B4B4`	|   Operators, operator functions  	|
|   ![19](img/19.png)	|   `#D7D700`	|   Fields  	|


# Example Screenshot

![Screenshot](https://image.prntscr.com/image/gXOA-RCTROaXTZkyIgpzDQ.png)

# Author

This dark theme was developed by delinsyl based on Visual Studio's default dark theme.
